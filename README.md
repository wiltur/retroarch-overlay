## Overlays pour Retroarch
Collection d'Overlays fait pour fonctionner avec les cores Mame sous Retroarch (le fichier de config associé à l'Overlay est prévu pour Retroarch, mais rien n'empĉhe de les utiliser avec un pur Mame).
Les Overlay sont inspirés et dérivés des bezels orignaux la plupart du temps, deux formats pour chaque set, 16/9 et 4/3, tous en réglage video "*Core provided*".

![1941-4-3](https://yargl.com/divers/thumb-1941-4-3.jpg)
![arkanoid-4-3](https://yargl.com/divers/thumb-arkanoid-4-3.jpg)
![dkong-16-9](https://yargl.com/divers/thumb-dkong-16-9.jpg)
![galaga-16-](https://yargl.com/divers/thumb-galaga-16-9.jpg)

**Instructions d'utilisation :**
  - Placer les fichiers (png et cfg) dans le dossier overlay de Retroarch ( ~/.config/retroarch/overlay)
  - lancer le jeu depuis Retroarch puis faire F1
  - descendre jusqu'à Onscreen Overlay
  - mettre DIsplay Overlay sur ON
  - aller sur Overlay Preset et selectionner le fichier correspondant au jeu (.cfg)
  - Mettre Overlay Opacity à 1.00
  - Faire Retour (backspace)
  - Descendre à Overrides
  - Selectionner le dernier (Save Game Overrides) pour que l'overlay soit associé définitivement au jeu.
  - Refaire F1 et vous pouvez jouer avec un écran moins nu !
  
